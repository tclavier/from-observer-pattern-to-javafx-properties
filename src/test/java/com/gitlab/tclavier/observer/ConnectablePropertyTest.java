package com.gitlab.tclavier.observer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConnectablePropertyTest {
    @Test
    public void should_be_observable() {
        ObservableProperty<String> observable = new ConnectableProperty<>();
    }

    @Test
    public void should_be_an_observer() {
        Observer<String> observer = new ConnectableProperty<>();
    }

    @Test
    public void should_init_value_in_constructor() {
        ConnectableProperty<Integer> p1 = new ConnectableProperty<>(42);
        assertEquals(42, p1.getValue());
    }

    @Test
    public void should_changes_value_of_connected_observer() {
        ConnectableProperty<Integer> p1 = new ConnectableProperty<>(42);
        ConnectableProperty<Integer> p2 = new ConnectableProperty<>();
        p2.bind(p1);
        assertEquals(42, p2.getValue());
    }

    @Test
    public void should_transmits_value_to_connected_observer() {
        ConnectableProperty<Integer> p1 = new ConnectableProperty<>();
        ConnectableProperty<Integer> p2 = new ConnectableProperty<>();
        p1.bind(p2);
        p2.setValue(5);

        assertEquals(5, p1.getValue());
        assertEquals(5, p2.getValue());
    }

    @Test
    public void test_connectTo_does_not_transmit_value_from_connected() {
        ConnectableProperty<Integer> p1 = new ConnectableProperty<>();
        ConnectableProperty<Integer> p2 = new ConnectableProperty<>(42);
        p1.bind(p2);
        assertEquals(42, p2.getValue());

        p1.setValue(5);
        assertEquals(42, p2.getValue());
    }


    @Test
    public void test_bindBidirectional_init_value_from_first() {
        ConnectableProperty<Integer> p1 = new ConnectableProperty<>(42);
        ConnectableProperty<Integer> p2 = new ConnectableProperty<>(14);
        p1.bindBidirectional(p2);

        assertEquals(42, p1.getValue());
        assertEquals(42, p2.getValue());
    }

    @Test
    public void test_bindBidirectional_transmits_value_both_way() {
        ConnectableProperty<Integer> p1 = new ConnectableProperty<>();
        ConnectableProperty<Integer> p2 = new ConnectableProperty<>();
        p1.bindBidirectional(p2);
        p1.setValue(42);

        assertEquals(42, p1.getValue());
        assertEquals(42, p2.getValue());

        p2.setValue(5);
        assertEquals(5, p1.getValue());
        assertEquals(5, p2.getValue());

        p1.setValue(12);
        assertEquals(12, p1.getValue());
        assertEquals(12, p2.getValue());
    }

    @Test
    public void test_circular_connects_do_not_loop() {
        ConnectableProperty<Integer> p1 = new ConnectableProperty<>();
        ConnectableProperty<Integer> p2 = new ConnectableProperty<>();
        ConnectableProperty<Integer> p3 = new ConnectableProperty<>();

        p1.bindBidirectional(p2);
        p2.bindBidirectional(p3);
        p3.bindBidirectional(p1);

        p2.setValue(5);

        assertEquals(5, p1.getValue());
        assertEquals(5, p2.getValue());
        assertEquals(5, p3.getValue());

        p1.setValue(42);

        assertEquals(42, p1.getValue());
        assertEquals(42, p2.getValue());
        assertEquals(42, p3.getValue());

        p3.setValue(17);

        assertEquals(17, p1.getValue());
        assertEquals(17, p2.getValue());
        assertEquals(17, p3.getValue());
    }

    @Test
    public void test_unconnected_property_does_not_transmit_value() {
        ConnectableProperty<Integer> p1 = new ConnectableProperty<>();
        ConnectableProperty<Integer> p2 = new ConnectableProperty<>(5);
        p1.bind(p2);
        assertEquals(5, p1.getValue());

        p1.unbind(p2);
        p2.setValue(42);

        assertEquals(5, p1.getValue());
    }

    @Test
    public void test_unconnected_property_does_not_transmit_value_2() {
        ConnectableProperty<Integer> p1 = new ConnectableProperty<>(5);
        ConnectableProperty<Integer> p2 = new ConnectableProperty<>();
        p1.bindBidirectional(p2);

        p1.unbindBidirectional(p2);
        p2.setValue(42);
        assertEquals(5, p1.getValue());
        assertEquals(42, p2.getValue());
        p1.setValue(7);
        assertEquals(7, p1.getValue());
        assertEquals(42, p2.getValue());
    }
}
