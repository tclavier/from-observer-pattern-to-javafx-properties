package com.gitlab.tclavier.observer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ObserverTest {

    @Test
    public void subject_should_be_observable() {
        Observable<Integer> subject = new SampleSubject();
    }

    @Test
    public void spectator_should_be_observer() {
        Observer<Integer> observer = new SampleSpectator();
        observer.update(5);
    }

    @Test
    public void spectator_should_be_notified_by_update_call() {
        SampleSpectator observer = new SampleSpectator();
        observer.update(5);
        assertTrue(observer.wasNotified());
    }

    @Test
    public void spectator_should_not_notified_without_update_call() {
        SampleSpectator observer = new SampleSpectator();
        assertFalse(observer.wasNotified());
    }

    @Test
    public void observer_should_be_attached_and_detached() {
        Observable<Integer> observable = new SampleSubject();
        Observer<Integer> observer = new SampleSpectator();
        observable.attach(observer);
        observable.detach(observer);
    }

    @Test
    public void should_not_notify_unattached_observer() {
        Observable<Integer> subject = new SampleSubject();
        subject.notifyObservers(5);
        assertFalse(new SampleSpectator().wasNotified());
    }

    @Test
    public void should_notifies_attached_observer() {
        Observable<Integer> subject = new SampleSubject();
        SampleSpectator spectator = new SampleSpectator();
        subject.attach(spectator);
        subject.notifyObservers(7);
        assertTrue(spectator.wasNotified());
    }

    @Test
    public void should_notifies_several_attached_observers() {
        Observable<Integer> subject = new SampleSubject();
        SampleSpectator spectator1 = new SampleSpectator();
        SampleSpectator spectator2 = new SampleSpectator();
        SampleSpectator spectator3 = new SampleSpectator();

        subject.attach(spectator1);
        subject.attach(spectator2);
        subject.attach(spectator3);
        subject.notifyObservers(5);

        assertTrue(spectator1.wasNotified());
        assertTrue(spectator2.wasNotified());
        assertTrue(spectator3.wasNotified());
    }

    @Test
    public void should_not_notify_observer_attached_then_detached() {
        Observable<Integer> subject = new SampleSubject();
        SampleSpectator spectator = new SampleSpectator();
        subject.attach(spectator);
        subject.detach(spectator);
        subject.notifyObservers(5);
        assertFalse(spectator.wasNotified());
    }

    @Test
    public void should_not_notify_observer_after_detach_without_attach() {
        Observable<Integer> subject = new SampleSubject();
        SampleSpectator spectator = new SampleSpectator();
        subject.detach(spectator);
        subject.notifyObservers(5);
        assertFalse(spectator.wasNotified());
    }

    @Test
    public void should_push_notify_transmits_value() {
        Observable<Integer> subject = new SampleSubject();
        SampleSpectator spectator = new SampleSpectator();
        subject.attach(spectator);
        assertEquals(0, spectator.getValue());
        subject.notifyObservers(5);
        assertTrue(spectator.wasNotified());
        assertEquals(5, spectator.getValue());
    }
}
