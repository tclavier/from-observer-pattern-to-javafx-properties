package com.gitlab.tclavier.observer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ObservablePropertyTest {
   @Test
    public void should_be_observable() {
        Observable<Integer> observable = new ObservableProperty<Integer>();
    }

     @Test
    public void test_change_ObservableProperty_without_observer() {
        ObservableProperty<Integer> property = new ObservableProperty<>();
        property.setValue(5);
        assertEquals(5, property.getValue());
    }

    @Test
    public void test_change_ObservableProperty_notifies_observer() {
        ObservableProperty<Integer> property = new ObservableProperty<>();
        SampleSpectator observer = new SampleSpectator();
        property.attach(observer);
        property.setValue(5);
        assertTrue(observer.wasNotified());
    }

    @Test
    public void test_change_ObservableProperty_does_not_notify_detached_observer() {
        ObservableProperty<Integer> property = new ObservableProperty<>();
        SampleSpectator observer = new SampleSpectator();

        property.attach(observer);
        property.detach(observer);
        property.setValue(5);
        assertFalse(observer.wasNotified());
    }
}
