package com.gitlab.tclavier.temperature;

import org.junit.jupiter.api.Test;

import static com.gitlab.tclavier.temperature.TemperatureUnit.*;
import static com.gitlab.tclavier.temperature.TemperatureUnit.Fahrenheit;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TemperatureTest {
    public static final double DELTA = 1e-10;

    @Test
    void should_init_temperature_from_mesure_and_unit() {
        Temperature temperature = TempFactory.from(0.0, Kelvin);
        assertEquals(0.0, temperature.getMesure(), DELTA);
        assertEquals(Kelvin, temperature.getUnit());
    }

    @Test
    void should_create_waterBoilingPoint_temperature() {
        assertEquals(100, TempFactory.waterBoilingPoint().getMesure());
        assertEquals(Celsius, TempFactory.waterBoilingPoint().getUnit());
    }

    @Test
    void should_create_waterMeltingPoint_temperature() {
        assertEquals(0, TempFactory.waterMeltingPoint().getMesure());
        assertEquals(Celsius, TempFactory.waterMeltingPoint().getUnit());
    }

    @Test
    void should_convert_unit() {
        Temperature actual = TempFactory.waterBoilingPoint().convertTo(TemperatureUnit.Fahrenheit);
        assertEquals(212, actual.getMesure());
        assertEquals(TemperatureUnit.Fahrenheit, actual.getUnit());
    }

    @Test
    void should_compare_with_different_units() {
        assertEquals(TempFactory.waterBoilingPoint(), TempFactory.from(373.15, Kelvin));
    }

    @Test
    void should_have_a_string_representation() {
        assertEquals("100.0 Celsius", TempFactory.waterBoilingPoint().toString());
    }

    @Test
    void shoudl_create_temperature_from_string() {
        assertEquals(TempFactory.waterMeltingPoint(), TempFactory.from("273.15", Kelvin));
    }

    @Test
    void should_compare_with_float_err() {
        assertEquals(TempFactory.from(42.0, Fahrenheit), TempFactory.from(41.999999999999995, Fahrenheit));
    }
}