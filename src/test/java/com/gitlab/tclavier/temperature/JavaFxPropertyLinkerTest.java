package com.gitlab.tclavier.temperature;

import com.gitlab.tclavier.observer.ConnectableProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JavaFxPropertyLinkerTest {
    @Test
    void should_transform_in_left_to_right() {
        JavaFxPropertyLinker<String, Integer> propertyLinker = new JavaFxPropertyLinker<>();
        StringProperty stringProperty = new SimpleStringProperty();
        ConnectableProperty<Integer> intProperty = new ConnectableProperty<>();

        propertyLinker.leftBindBidirectional(stringProperty);
        propertyLinker.rightBindBidirectional(intProperty);
        propertyLinker.setConverter(Integer::parseInt);
        propertyLinker.setRevertConverter(Object::toString);

        stringProperty.setValue("17");
        assertEquals(17, intProperty.getValue());
    }

    @Test
    void should_transform_from_right_to_left() {
        PropertyLinker<String, Integer> propertyLinker = new PropertyLinker<>();
        ConnectableProperty<String> stringProperty = new ConnectableProperty<>();
        ConnectableProperty<Integer> intProperty = new ConnectableProperty<>();

        propertyLinker.leftBindBidirectional(stringProperty);
        propertyLinker.rightBindBidirectional(intProperty);
        propertyLinker.setConverter(Integer::parseInt);
        propertyLinker.setRevertConverter(Object::toString);

        intProperty.setValue(42);
        assertEquals("42", stringProperty.getValue());
    }
}