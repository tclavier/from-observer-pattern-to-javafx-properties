package com.gitlab.tclavier.temperature;

import com.gitlab.tclavier.observer.Observer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PropertyListenerTest {

    @Test
    void listener_is_an_observer() {
        Observer<Temperature> listener = new PropertyListener<>();
    }

    @Test
    void should_exec_a_callback_on_update() {
        PropertyListener<Temperature> listener = new PropertyListener<>();
        var temperatureString = new Object() {
            String value = "";
        };
        listener.setOnUpdate((Temperature t) -> temperatureString.value = t.toString());
        listener.update(TempFactory.waterBoilingPoint());
        assertEquals("100.0 Celsius", temperatureString.value);
    }
}