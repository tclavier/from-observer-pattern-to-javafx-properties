package com.gitlab.tclavier.temperature;

import javafx.beans.property.Property;
import javafx.beans.property.SimpleDoubleProperty;
import org.junit.jupiter.api.Test;

import static com.gitlab.tclavier.temperature.TemperatureUnit.Fahrenheit;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ViewModelSliderTest {
    public static final double DELTA = 1e-10;

    @Test
    public void should_show_domain_model_udpdate() {
        TemperatureModel domainModel = new TemperatureModel(TempFactory.waterMeltingPoint());
        ViewModelSlider viewModelLabel = new ViewModelSlider(domainModel, Fahrenheit);
        assertEquals("Température en Fahrenheit", viewModelLabel.getTitle());
        domainModel.setValue(TempFactory.waterMeltingPoint());
        assertEquals(32.0, viewModelLabel.getNumberTemperature());
    }

    @Test
    public void should_update_domain_model() {
        TemperatureModel domainModel = new TemperatureModel(TempFactory.waterBoilingPoint());
        ViewModelSlider viewModelLabel = new ViewModelSlider(domainModel, Fahrenheit);
        viewModelLabel.setDoubleTemperature(32.0);
        assertEquals(TempFactory.waterMeltingPoint(), domainModel.getTemperatureIn(Fahrenheit));
    }

    @Test
    public void should_update_domain_model_with_ConnectableProperty() {
        TemperatureModel domainModel = new TemperatureModel(TempFactory.waterMeltingPoint());
        ViewModelSlider viewModelLabel = new ViewModelSlider(domainModel, Fahrenheit);
        Property<Number> property = new SimpleDoubleProperty();
        viewModelLabel.doubleProperty().bindBidirectional(property);
        property.setValue(17.0);
        assertEquals(TempFactory.from(17.0, Fahrenheit), domainModel.getTemperatureIn(Fahrenheit));
    }

    @Test
    public void should_update_ConnectableProperty_from_model() {
        TemperatureModel domainModel = new TemperatureModel(TempFactory.waterMeltingPoint());
        ViewModelSlider viewModelLabel = new ViewModelSlider(domainModel, Fahrenheit);
        Property<Number> property = new SimpleDoubleProperty();
        viewModelLabel.doubleProperty().bindBidirectional(property);
        domainModel.setValue(TempFactory.from(24.0, Fahrenheit));
        assertEquals(24, property.getValue());
    }
    @Test
    public void should_provide_max_value() {
        TemperatureModel domainModel = new TemperatureModel(TempFactory.waterMeltingPoint());
        ViewModelSlider viewModelLabel = new ViewModelSlider(domainModel, Fahrenheit);
        assertEquals(302, viewModelLabel.getMax());
    }
    @Test
    public void should_provide_min_value() {
        TemperatureModel domainModel = new TemperatureModel(TempFactory.waterMeltingPoint());
        ViewModelSlider viewModelLabel = new ViewModelSlider(domainModel, Fahrenheit);
        assertEquals(-58, viewModelLabel.getMin());
    }
}
