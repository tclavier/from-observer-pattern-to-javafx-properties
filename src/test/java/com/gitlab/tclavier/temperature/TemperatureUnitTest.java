package com.gitlab.tclavier.temperature;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TemperatureUnitTest {
    @Test
    void should_convert_to_kelvin() {
        assertEquals(0.0, TemperatureUnit.Kelvin.toKelvin(0.0));
        assertEquals(273.15, TemperatureUnit.Celsius.toKelvin(0.0));
        assertEquals(273.15, TemperatureUnit.Fahrenheit.toKelvin(32.0));
    }

    @Test
    void should_convert_from_kelvin() {
        assertEquals(10.0, TemperatureUnit.Kelvin.fromKelvin(10.0));
        assertEquals(0, TemperatureUnit.Celsius.fromKelvin(273.15));
        assertEquals(32, TemperatureUnit.Fahrenheit.fromKelvin(273.15));
    }

}