package com.gitlab.tclavier.temperature;

import com.gitlab.tclavier.observer.ObservableProperty;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TemperatureModelTest {
    public static final double DELTA = 1e-10;

    private TemperatureModel initModelAtZeroCelsius() {
        return new TemperatureModel(TempFactory.waterMeltingPoint());
    }

    @Test
    void should_be_connectable() {
        ObservableProperty<Temperature> property = new TemperatureModel(TempFactory.waterMeltingPoint());
        Temperature temperature = property.getValue();
        assertEquals(TempFactory.waterMeltingPoint(), temperature);
    }

    @Test
    public void should_set_temperature_in_Kelvin_and_read_in_celcius() {
        TemperatureModel model = new TemperatureModel(TempFactory.from(100.0, TemperatureUnit.Kelvin));
        assertEquals(-173.1499999999, model.getTemperatureIn(TemperatureUnit.Celsius).getMesure(), DELTA);
        assertEquals(TemperatureUnit.Celsius, model.getTemperatureIn(TemperatureUnit.Celsius).getUnit());
    }

    @Test
    public void should_set_temperature_in_celsius_and_read_in_kelvin() {
        TemperatureModel model = initModelAtZeroCelsius();
        assertEquals(273.15, model.getTemperatureIn(TemperatureUnit.Kelvin).getMesure(), DELTA);
        assertEquals(TemperatureUnit.Kelvin, model.getTemperatureIn(TemperatureUnit.Kelvin).getUnit());
    }

    @Test
    public void should_set_temperature_in_celsius_and_read_in_fahrenheit() {
        TemperatureModel model = initModelAtZeroCelsius();
        assertEquals(32, model.getTemperatureIn(TemperatureUnit.Fahrenheit).getMesure(), DELTA);
    }

    @Test
    public void should_set_temperature_in_fahrenheit_and_read_in_celcius() {
        TemperatureModel model = new TemperatureModel(TempFactory.from(100.0, TemperatureUnit.Fahrenheit));
        assertEquals(37.7777777777, model.getTemperatureIn(TemperatureUnit.Celsius).getMesure(), DELTA);
    }

    @Test
    void should_increase_temperature_by_1() {
        TemperatureModel model = initModelAtZeroCelsius();
        model.increase(TemperatureUnit.Celsius);
        assertEquals(1, model.getTemperatureIn(TemperatureUnit.Celsius).getMesure(), DELTA);
    }

    @Test
    void should_increase_temperature_by_1_in_fahrenheit() {
        TemperatureModel model = initModelAtZeroCelsius();
        model.increase(TemperatureUnit.Fahrenheit);
        assertEquals(0.555555555555, model.getTemperatureIn(TemperatureUnit.Celsius).getMesure(), DELTA);
    }

    @Test
    void should_decrease_temperature_by_1() {
        TemperatureModel model = initModelAtZeroCelsius();
        model.decrease(TemperatureUnit.Celsius);
        assertEquals(-1, model.getTemperatureIn(TemperatureUnit.Celsius).getMesure(), DELTA);
    }

    @Test
    void should_decrease_temperature_by_1_in_fahrenheit() {
        TemperatureModel model = initModelAtZeroCelsius();
        model.decrease(TemperatureUnit.Fahrenheit);
        assertEquals(-0.555555555555, model.getTemperatureIn(TemperatureUnit.Celsius).getMesure(), DELTA);
    }

    @Test
    void should_have_a_max_at_150C() {
        TemperatureModel model = initModelAtZeroCelsius();
        assertEquals(TempFactory.from(150.0, TemperatureUnit.Celsius), model.getMax());
    }


    @Test
    void should_have_a_min_at_50C() {
        TemperatureModel model = initModelAtZeroCelsius();
        assertEquals(TempFactory.from(-50.0, TemperatureUnit.Celsius), model.getMin());
    }
}