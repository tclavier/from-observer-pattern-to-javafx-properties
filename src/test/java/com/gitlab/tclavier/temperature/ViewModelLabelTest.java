package com.gitlab.tclavier.temperature;

import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import org.junit.jupiter.api.Test;

import static com.gitlab.tclavier.temperature.TemperatureUnit.Celsius;
import static com.gitlab.tclavier.temperature.TemperatureUnit.Fahrenheit;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ViewModelLabelTest {
    private ViewModelLabel buildSampleViewModel() {
        TemperatureModel temperatureModel = new TemperatureModel(TempFactory.waterMeltingPoint());
        return new ViewModelLabel(temperatureModel, Celsius);
    }

    @Test
    public void domain_model_should_provide_temperature_in_unit_as_string() {
        TemperatureModel domainModel = new TemperatureModel(TempFactory.waterMeltingPoint());
        TemperatureUnit displayUnit = Celsius;
        ViewModelLabel viewModelLabel = new ViewModelLabel(domainModel, displayUnit);
        assertEquals("Température en Celsius", viewModelLabel.getTitle());
        assertEquals("0.0", viewModelLabel.getStringTemperature());
    }

    @Test
    public void should_bind_to_stringRepresentation() {
        ViewModelLabel viewModelLabel = buildSampleViewModel();
        Property<String> property = new SimpleStringProperty();
        viewModelLabel.stringProperty().bindBidirectional(property);
        property.setValue("12.0");
        assertEquals("12.0", viewModelLabel.getStringTemperature());
    }

    @Test
    public void should_show_domain_model() {
        TemperatureModel domainModel = new TemperatureModel(TempFactory.waterMeltingPoint());
        ViewModelLabel viewModelLabel = new ViewModelLabel(domainModel, Celsius);
        domainModel.setValue(TempFactory.from(10.0, Celsius));
        assertEquals("10.0", viewModelLabel.getStringTemperature());
    }

    @Test
    public void should_update_domain_model() {
        TemperatureModel domainModel = new TemperatureModel(TempFactory.waterMeltingPoint());
        ViewModelLabel viewModelLabel = new ViewModelLabel(domainModel, Celsius);
        viewModelLabel.setTemperature("100");
        assertEquals(TempFactory.waterBoilingPoint(), domainModel.getTemperatureIn(Celsius));
    }

    @Test
    public void should_have_one_increment_action() {
        ViewModelLabel viewModelLabel = buildSampleViewModel();
        viewModelLabel.increase();
        assertEquals("1.0", viewModelLabel.getStringTemperature());
    }

    @Test
    public void should_have_one_decrease_action() {
        ViewModelLabel viewModelLabel = buildSampleViewModel();
        viewModelLabel.decrease();
        assertEquals("-1.0", viewModelLabel.getStringTemperature());
    }

    @Test
    public void should_use_the_display_unit() {
        TemperatureModel domainModel = new TemperatureModel(TempFactory.waterMeltingPoint());
        ViewModelLabel viewModelLabel = new ViewModelLabel(domainModel, Fahrenheit);
        domainModel.setValue(TempFactory.from(10.0, Celsius));
        assertEquals("50.0", viewModelLabel.getStringTemperature());
    }

    @Test
    public void should_update_domain_model_with_ConnectableProperty() {
        TemperatureModel domainModel = new TemperatureModel(TempFactory.waterMeltingPoint());
        ViewModelLabel viewModelLabel = new ViewModelLabel(domainModel, Fahrenheit);
        Property<String> property = new SimpleStringProperty();
        viewModelLabel.stringProperty().bindBidirectional(property);
        property.setValue("42");
        assertEquals(TempFactory.from(42.0, Fahrenheit), domainModel.getTemperatureIn(Fahrenheit));
    }

    @Test
    public void should_update_ConnectableProperty_from_model() {
        TemperatureModel domainModel = new TemperatureModel(TempFactory.waterBoilingPoint());
        ViewModelLabel viewModelLabel = new ViewModelLabel(domainModel, Fahrenheit);
        Property<String> property = new SimpleStringProperty();
        viewModelLabel.stringProperty().bindBidirectional(property);
        domainModel.setValue(TempFactory.from(32.3, Fahrenheit));
        assertEquals("32.3", property.getValue().substring(0, 4));
    }

    @Test
    public void should_be_init_from_model() {
        ViewModelLabel viewModelLabel = buildSampleViewModel();
        assertEquals("0.0", viewModelLabel.getStringTemperature());
    }
}
