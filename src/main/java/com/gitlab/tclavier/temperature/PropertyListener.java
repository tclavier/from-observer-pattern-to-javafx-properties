package com.gitlab.tclavier.temperature;

import com.gitlab.tclavier.observer.Observer;

import java.util.function.Consumer;

public class PropertyListener<T> implements Observer<T> {
    private Consumer<T> onUpdate;

    @Override
    public void update(T value) {
        onUpdate.accept(value);
    }

    public void setOnUpdate(Consumer<T> onUpdate) {
        this.onUpdate = onUpdate;
    }
}
