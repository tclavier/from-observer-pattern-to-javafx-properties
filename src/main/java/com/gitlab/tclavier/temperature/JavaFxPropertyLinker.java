package com.gitlab.tclavier.temperature;

import com.gitlab.tclavier.observer.ConnectableProperty;
import javafx.beans.property.Property;

import java.util.function.Function;

public class JavaFxPropertyLinker<L, R> {
    private Function<R, L> right2left;
    private ConnectableProperty<R> right;
    private Property<L> left;
    private Function<L, R> left2right;

    public void leftBindBidirectional(Property<L> left) {
        this.left = left;
        left.addListener((observableValue, oldValue, newValue) -> right.setValue(left2right.apply(newValue)));
    }

    public void rightBindBidirectional(ConnectableProperty<R> right) {
        this.right = right;
        PropertyListener<R> listener = new PropertyListener<>();
        right.attach(listener);
        listener.setOnUpdate(v -> left.setValue(right2left.apply(v)));
    }

    public void setConverter(Function<L, R> left2right) {
        this.left2right = left2right;
    }

    public void setRevertConverter(Function<R, L> right2left) {
        this.right2left = right2left;
    }
}
