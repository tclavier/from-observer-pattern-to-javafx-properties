package com.gitlab.tclavier.temperature;

import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.stage.Stage;

import static javafx.geometry.Orientation.VERTICAL;

public class ViewSlider {
    private Slider slider;
    private ViewModelSlider viewModel;

    public ViewSlider(ViewModelSlider viewModel) {
        this.viewModel = viewModel;
    }

    public void start(Stage stage) {
        slider = new Slider(viewModel.getMin(), viewModel.getMax(), viewModel.getNumberTemperature());
        slider.valueProperty().bindBidirectional(viewModel.doubleProperty());
        slider.setOrientation(VERTICAL);
        slider.setShowTickMarks(true);
        slider.setShowTickLabels(true);
        slider.setValueChanging(true);
        slider.setMajorTickUnit(10);
        slider.setMinorTickCount(5);
        Scene scene = new Scene(slider);
        stage.setTitle(viewModel.getTitle());
        stage.setWidth(150);
        stage.setHeight(400);
        stage.setScene(scene);
        stage.show();
    }


}
