package com.gitlab.tclavier.temperature;

import com.gitlab.tclavier.temperature.Temperature;
import com.gitlab.tclavier.temperature.TemperatureUnit;

import static com.gitlab.tclavier.temperature.TemperatureUnit.Celsius;

public class TempFactory {
    public static Temperature waterMeltingPoint() {
        return from(0.0, Celsius);
    }

    public static Temperature waterBoilingPoint() {
        return from(100.0, Celsius);
    }

    public static Temperature from(String s, TemperatureUnit unit) {
        return new Temperature(Double.parseDouble(s), unit);
    }

    public static Temperature from(Double v, TemperatureUnit unit) {
        return new Temperature(v, unit);
    }
}
