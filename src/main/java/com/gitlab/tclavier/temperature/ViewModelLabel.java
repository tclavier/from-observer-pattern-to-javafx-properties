package com.gitlab.tclavier.temperature;

import com.gitlab.tclavier.observer.ConnectableProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;

public class ViewModelLabel {
    private TemperatureModel domainModel;
    private TemperatureUnit displayUnit;
    private Property<String> stringProperty = new SimpleStringProperty();

    public ViewModelLabel(TemperatureModel domainModel, TemperatureUnit displayUnit) {
        this.domainModel = domainModel;
        this.displayUnit = displayUnit;
        stringProperty.setValue(domainModel.getValue().convertTo(displayUnit).getMesure().toString());
        JavaFxPropertyLinker<String, Temperature> domainLinker = new JavaFxPropertyLinker<>();
        domainLinker.leftBindBidirectional(stringProperty);
        domainLinker.rightBindBidirectional(domainModel);
        domainLinker.setRevertConverter(t -> t.convertTo(displayUnit).getMesure().toString());
        domainLinker.setConverter(s -> TempFactory.from(s, displayUnit));
    }

    public String getTitle() {
        return "Température en " + displayUnit;
    }

    public String getStringTemperature() {
        return stringProperty.getValue();
    }

    public Property<String> stringProperty() {
        return stringProperty;
    }

    public void setTemperature(String s) {
        stringProperty.setValue(s);
    }

    public void increase() {
        domainModel.increase(displayUnit);
    }

    public void decrease() {
        domainModel.decrease(displayUnit);
    }
}
