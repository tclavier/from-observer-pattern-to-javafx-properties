package com.gitlab.tclavier.temperature;

import static com.gitlab.tclavier.temperature.TemperatureUnit.Kelvin;

public class Temperature {
    private final double mesure;
    private final TemperatureUnit unit;

    public Temperature(double mesure, TemperatureUnit unit) {
        this.mesure = mesure;
        this.unit = unit;
    }

    public Temperature convertTo(TemperatureUnit toUnit) {
        Temperature kelvin = TempFactory.from(unit.toKelvin(mesure), Kelvin);
        return TempFactory.from(toUnit.fromKelvin(kelvin.getMesure()), toUnit);
    }

    public TemperatureUnit getUnit() {
        return unit;
    }

    public Double getMesure() {
        return mesure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Temperature that = (Temperature) o;
        return Math.round(that.convertTo(unit).getMesure() * 1000) == Math.round(mesure * 1000);
    }

    @Override
    public String toString() {
        return mesure + " " + unit;
    }

}
