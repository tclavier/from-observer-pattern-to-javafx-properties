package com.gitlab.tclavier.temperature;

import java.util.function.Function;

public enum TemperatureUnit {
    Kelvin(t -> t, t -> t),
    Celsius(t -> t + 273.15, t -> t - 273.15),
    Fahrenheit(t -> (t - 32) * 5 / 9 + 273.15, t -> (t - 273.15) * 9 / 5 + 32);

    private final Function<Double, Double> convertToK;
    private final Function<Double, Double> convertfromK;

    TemperatureUnit(Function<Double, Double> convertToK, Function<Double, Double> convertfromK) {
        this.convertToK = convertToK;
        this.convertfromK = convertfromK;
    }

    public double toKelvin(double v) {
        return convertToK.apply(v);
    }

    public double fromKelvin(double v) {
        return convertfromK.apply(v);
    }
}
