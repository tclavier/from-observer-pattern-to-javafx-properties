package com.gitlab.tclavier.temperature;

import com.gitlab.tclavier.observer.ConnectableProperty;

public class TemperatureModel extends ConnectableProperty<Temperature> {
    public TemperatureModel(Temperature temperature) {
        setValue(temperature);
    }

    public Temperature getTemperatureIn(TemperatureUnit unit) {
        return getValue().convertTo(unit);
    }

    public void increase(TemperatureUnit unit) {
        Temperature inUnit = getValue().convertTo(unit);
        setValue(TempFactory.from(inUnit.getMesure() + 1, unit));
    }

    public void decrease(TemperatureUnit unit) {
        Temperature inUnit = getValue().convertTo(unit);
        setValue(TempFactory.from(inUnit.getMesure() - 1, unit));
    }

    public Temperature getMax() {
        return TempFactory.from(150.0, TemperatureUnit.Celsius);
    }

    public Temperature getMin() {
        return TempFactory.from(-50.0, TemperatureUnit.Celsius);
    }
}
