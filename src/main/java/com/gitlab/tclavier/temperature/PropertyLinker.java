package com.gitlab.tclavier.temperature;

import com.gitlab.tclavier.observer.ConnectableProperty;

import java.util.function.Function;

public class PropertyLinker<L, R> {
    private ConnectableProperty<L> left;
    private ConnectableProperty<R> right;
    private Function<L, R> left2right;
    private Function<R, L> right2left;

    public void leftBindBidirectional(ConnectableProperty<L> left) {
        this.left = left;
        PropertyListener<L> listener = new PropertyListener<>();
        left.attach(listener);
        listener.setOnUpdate(l -> right.setValue(left2right.apply(l)));
    }

    public void rightBindBidirectional(ConnectableProperty<R> right) {
        this.right = right;
        PropertyListener<R> listener = new PropertyListener<>();
        right.attach(listener);
        listener.setOnUpdate(r -> left.setValue(right2left.apply(r)));
    }

    public void setRevertConverter(Function<R, L> right2left) {
        this.right2left = right2left;
    }

    public void setConverter(Function<L, R> left2right) {
        this.left2right = left2right;
    }
}
