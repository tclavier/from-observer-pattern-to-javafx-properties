package com.gitlab.tclavier.temperature;

import com.gitlab.tclavier.observer.ConnectableProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleDoubleProperty;

public class ViewModelSlider {
    private TemperatureModel domainModel;
    private TemperatureUnit displayUnit;
    private Property<Number> doubleValue = new SimpleDoubleProperty();

    public ViewModelSlider(TemperatureModel domainModel, TemperatureUnit displayUnit) {
        this.domainModel = domainModel;
        this.displayUnit = displayUnit;
        doubleValue.setValue(domainModel.getValue().convertTo(displayUnit).getMesure());
        JavaFxPropertyLinker<Number, Temperature> domainLinker = new JavaFxPropertyLinker<>();
        domainLinker.rightBindBidirectional(domainModel);
        domainLinker.leftBindBidirectional(doubleValue);
        domainLinker.setConverter(d -> TempFactory.from(d.doubleValue(), displayUnit));
        domainLinker.setRevertConverter(t -> t.convertTo(displayUnit).getMesure());
    }

    public String getTitle() {
        return "Température en " + displayUnit;
    }

    public double getNumberTemperature() {
        return doubleValue.getValue().doubleValue();
    }

    public void setDoubleTemperature(double v) {
        doubleValue.setValue(v);
    }

    public Property<Number> doubleProperty() {
        return doubleValue;
    }

    public double getMax() {
        return domainModel.getMax().convertTo(displayUnit).getMesure();
    }

    public double getMin() {
        return domainModel.getMin().convertTo(displayUnit).getMesure();
    }
}
