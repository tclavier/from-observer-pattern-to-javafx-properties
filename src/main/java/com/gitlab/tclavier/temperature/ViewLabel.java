package com.gitlab.tclavier.temperature;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class ViewLabel {

    private final TextField label = new TextField();
    private ViewModelLabel viewModel;

    public ViewLabel(ViewModelLabel viewModel) {
        this.viewModel = viewModel;
    }


    public void start(Stage stage) {
        label.setPrefWidth(300);
        label.textProperty().bindBidirectional(viewModel.stringProperty());
        Button plus = new Button("+");
        plus.setOnAction(e -> viewModel.increase());
        Button minus = new Button("-");
        minus.setOnAction(e -> viewModel.decrease());
        HBox hBox = new HBox(minus, label, plus);
        Scene scene = new Scene(hBox);
        stage.setTitle(viewModel.getTitle());
        stage.setScene(scene);
        stage.show();
    }

}
