package com.gitlab.tclavier.temperature;

import javafx.application.Application;
import javafx.stage.Stage;

import static com.gitlab.tclavier.temperature.TemperatureUnit.*;

public class Main extends Application {

    @Override
    public void start(Stage stage) {
        TemperatureModel model = new TemperatureModel(TempFactory.waterMeltingPoint());
        ViewLabel labelC = new ViewLabel(new ViewModelLabel(model, Celsius));
        labelC.start(stage);
        ViewLabel labelF = new ViewLabel(new ViewModelLabel(model, Fahrenheit));
        labelF.start(new Stage());
        ViewLabel labelK = new ViewLabel(new ViewModelLabel(model, Kelvin));
        labelK.start(new Stage());
        ViewSlider sliderC = new ViewSlider(new ViewModelSlider(model, Celsius));
        sliderC.start(new Stage());
        ViewSlider sliderF = new ViewSlider(new ViewModelSlider(model, Fahrenheit));
        sliderF.start(new Stage());
    }
}
