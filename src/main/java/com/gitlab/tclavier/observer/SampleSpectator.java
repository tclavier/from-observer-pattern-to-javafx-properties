package com.gitlab.tclavier.observer;

public class SampleSpectator implements Observer<Integer> {

    private boolean notified = false;
    private Integer i = 0;

    @Override
    public void update(Integer i) {
        this.i = i;
        notified = true;
    }

    public boolean wasNotified() {
        return this.notified;
    }

    public int getValue() {
        return i;
    }
}
