package com.gitlab.tclavier.observer;

public interface Observer<T> {
    void update(T i);
}
