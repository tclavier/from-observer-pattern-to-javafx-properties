package com.gitlab.tclavier.observer;

public class ConnectableProperty<T> extends ObservableProperty<T> implements Observer<T> {
    public ConnectableProperty() {

    }

    public ConnectableProperty(T value) {
        setValue(value);
    }

    @Override
    public void update(T value) {
        setValue(value);
    }

    public void bind(ConnectableProperty<T> property) {
        this.setValue(property.getValue());
        property.attach(this);
    }

    public void bindBidirectional(ConnectableProperty<T> property) {
        property.setValue(getValue());
        property.attach(this);
        this.attach(property);
    }

    public void unbind(ConnectableProperty<T> property) {
        property.detach(this);
    }

    public void unbindBidirectional(ConnectableProperty<T> property) {
        unbind(property);
        this.detach(property);
    }
}
